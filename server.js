/*jshint node:true*/
/////////////////////////////
//R&E Media Javascript test
//author: Saša Pul
//date	: 2015-02-25
/////////////////////////////

//server.js

//LOAD MODULES AND SETUP EXPRESS
//=============================

var express 	= require('express'),
		apiServer	= require('./apiserver'),
		app				= express();

//set a location for jaade templates and jade as rendering engine
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');

//set a location for client side scripts and css
app.use(express.static(__dirname + '/public'));

//disable extra headers
app.disable('x-powered-by');
app.disable('etag');

//INIT VARIABLES
//============================
var shops = ["Amazon", "Ebay", "Redcoon", "Apple"],
		port = 3000,
		apiPort = 5000;


//HANDLE REQUESTS
//============================

app.get('/', function(req, res){
	res.render('index', {shops: shops});
});



//START SERVER
//============================
app.listen(port);

console.log("webApp listening on port " + port);

apiServer.startApiServer(apiPort);