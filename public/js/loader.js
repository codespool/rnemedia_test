//SET JSONP
function getJsonpData() {
	
	//create new script element with src pointing to API and put it in the head 
	var apiUrl = "http://localhost:5000?callback=addLocaleData",
			script = document.createElement('script');
	
	script.setAttribute('src', apiUrl);
	
	document.getElementsByTagName('head')[0].appendChild(script);
};
	
//JSONP callback - insert data into DOM 
function addLocaleData(response) {
	
	//itterate over object
	for(var store in response) {
		if(response.hasOwnProperty(store)){
			
			
			//append new ul to each store
			var storeListing = document.getElementById(store);
			
			//prevent duplicating (remove all but text node)
			storeListing.innerHTML = store;
			
			var newUl = document.createElement('ul');
			storeListing.appendChild(newUl);
			
			//itterate over locales arrays
			for(var i = 0, iMax = response[store].length; i < iMax; i += 1){
				
				//create new li elements and text nodes from locales and append to nested ul
				var newLi = document.createElement('li');
				var locale = document.createTextNode(response[store][i]);
				
				newLi.appendChild(locale);
				newUl.appendChild(newLi);
			}
		}
	}
	
	
};


//add click event listener to the button
document.getElementById("loadData").addEventListener('click', getJsonpData);