#R&E Media Javascript developer test
*Author: Saša Pul*

*Date: 2015-25-02*


##Task:
- Create webApp server and API server using express
- webApp serves jade HTML template
- on button click loads json from API
- adds new elements to the DOM from JSON data received

###Notes:
- no javscript framework was used for frontend
- styling is minimum, as none is required
- jsonp used as no aditional headers are allowed
	
	
##Installation and usage	

clone this repo:

	https://codespool@bitbucket.org/codespool/rnemedia_test.git

install dependencies:
	
    npm install
run: 
		
	node server.js
go to url:

	localhost:3000
