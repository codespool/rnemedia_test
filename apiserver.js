/*jshint node:true*/

//apiserver.js


module.exports = {
	startApiServer : function(port) {
		
//load express and the locales json file
		var express = require('express'),
				api = express(),
				locales = require('./data/locales.json');
		
		//disable extra headers
		api.disable('x-powered-by');
		api.disable('etag');
		
//HANDLE REQUESTS
//=======================================
		
		//single endpoint with jsonp response
		api.get('/', function(req, res){
			res.jsonp(locales);
		});
		
//START SERVER
//=======================================
		api.listen(port);
		console.log("Api listening on port " + port);
		
	}
};